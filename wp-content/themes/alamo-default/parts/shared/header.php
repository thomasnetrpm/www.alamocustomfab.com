<!--Site Header-->
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/search-module' ) ); ?>

 <header class="site-header" role="banner">

      <div class="site-utility-nav">
        <a href="#" class="sh-ph">(210)-531-4132</a>
        <span class="sh-icons">           
          <a href="#menu" class="sh-ico-menu menu-link"><!-- <span>Menu</span> --></a>
        </span>
      </div>
      <div class="mobile-header">
        <div class="inner-wrap">
            <a href="<?php bloginfo('url') ?>" class="site-logo"><img src="<?php bloginfo('template_url') ?>/img/site-logo.png" alt="Site Logo"></a>
        </div>
      </div>
      <div class="inner-wrap">
        <!--Site Nav-->
        <div class="site-nav-container">
          <div class="snc-header">
            <a href="" class="close-menu menu-link">Close</a>
          </div>
          <?php wp_nav_menu(array(
                'menu'            => 'Primary Nav',
                'container'       => 'nav',
                'container_class' => 'site-nav',
                'menu_class'      => 'sn-level-1',
                'walker'        => new themeslug_walker_nav_menu
                )); ?>
        </div><!--Site Nav Container END-->
        <a href="" class="site-nav-container-screen menu-link">&nbsp;</a>

        
      </div><!--inner-wrap END-->
    </header>


<!--Site Content-->
<section class="site-content" role="main">