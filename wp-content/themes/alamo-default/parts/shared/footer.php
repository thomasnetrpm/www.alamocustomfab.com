</section>

<!--Site Footer-->
<!-- Values are set in the options tab of wordpress -->
    <footer class="site-footer" role="contentinfo">
      <div class="inner-wrap sf-main">
        <div class="sf-cta-block">
          <h2 class="sf-title"><?php the_field('sf_title', 'options');?></h2>
          <p class="sf-support"><?php the_field('sf_description', 'options');?></p>
          <a href="<?php the_field('sf_learn_more_url', 'options');?>" class="sf-cta btn">Learn More</a>
        </div>
        <div class="sf-contact-block">
          <img class="sf-texas" src="<?php bloginfo('template_url') ?>/img/footer-about-texas-outline.png" alt="Texas" />
          <p><?php the_field('sf_address_1', 'options');?> <br> <?php the_field('sf_address_2', 'options');?></p>
          <p><?php the_field('sf_phone', 'options');?><br> <?php the_field('sf_fax', 'options');?></p>
          <p><a href="mailto:<?php the_field('sf_email', 'options');?>"><?php the_field('sf_email', 'options');?></a></p>
        </div>
      </div>
      <div class="sf-small">
        <div class="inner-wrap">
          <p><a href="/sitemap">Sitemap</a> | <a href="/privacy-policy">Privacy Policy</a> | &copy; <?php echo date("Y"); ?> <?php bloginfo( 'name' ); ?> | Website by <a href="business.thomasnet.com/rpm">ThomasNet RPM</a></p>
        </div>
      </div>
    </footer>
</div><!-- site wrap close -->

