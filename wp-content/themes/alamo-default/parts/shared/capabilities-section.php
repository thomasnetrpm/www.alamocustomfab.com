<section class="capabilities-section">
  <div class="inner-wrap">
    <a class="cs-cap-category cs-main-category" href="/capabilities/"><img class="cs-pic" src="<?php bloginfo('template_url') ?>/img/all-capabilities.png" /><span class="cs-title">Our Capabilities</span></a>
    
		<?php if( have_rows('cs_bucket') ): ?>
			<?php while( have_rows('cs_bucket') ): the_row(); ?>
		    <a class="cs-cap-category" href="<?php the_sub_field('cs_link_url'); ?>"><img src="<?php the_sub_field('cs_pic'); ?>" alt="" /><span class="cs-title"><?php the_sub_field('cs_title'); ?></span></a>
			<?php endwhile; ?>
		<?php endif; ?>
  </div>
</section>