
<section class="site-intro">
  <div class="flexslider">
    <?php if( have_rows('home_slider') ): ?>
      <ul class="slides">
        <?php while( have_rows('home_slider') ): the_row(); ?>
        <li>
          <img src="<?php the_sub_field('home_slide'); ?>" alt="" />

        </li>
        <?php endwhile; ?>
  <!--       <li>
          <img class="slide" src="<?php bloginfo('template_url') ?>/img/main-image.jpg" />
        </li>
        <li>
          <img class="slide" src="<?php bloginfo('template_url') ?>/img/main-image.jpg" />
        </li>
        <li>
          <img class="slide" src="<?php bloginfo('template_url') ?>/img/main-image.jpg" />
        </li> -->
      </ul>
    <?php endif; ?>
  </div>
  <div class="inner-wrap">
    <div class="si-statement-block">
      <h2 class="si-title"><?php the_field('si_title');?></h2>
      <p class="si-title-support"><?php the_field('si_supporting_text'); ?></p>
    </div>
  </div>
</section>