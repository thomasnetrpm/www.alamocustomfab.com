<?php if($post->post_parent == '8' ):
$pagelist = get_pages('sort_column=menu_order&sort_order=asc&child_of=8');
$pages = array();
foreach ($pagelist as $page) {
   $pages[] += $page->ID;
}

$current = array_search(get_the_ID(), $pages);
$prevID = $pages[$current-1];
$nextID = $pages[$current+1];

if (!empty($nextID)) { ?>
<p id="last"></p>
<div id="slidebox"><a class="btn-slide-in" href="<?php echo get_permalink($nextID); ?>"><?php echo get_the_title($nextID); ?></a></div>
<?php } 
else { ?>
<p id="last"></p>
<div id="slidebox"><a class="btn-slide-in" href="<?php echo get_permalink($pages[0]); ?>"><?php echo get_the_title($pages[0]); ?></a></div>
<?php
}
?>

<?php endif; ?>


<?php if($post->post_parent == '58' ):
$pagelist = get_pages('sort_column=menu_order&sort_order=asc&child_of=58');
$pages = array();
foreach ($pagelist as $page) {
   $pages[] += $page->ID;
}

$current = array_search(get_the_ID(), $pages);
$prevID = $pages[$current-1];
$nextID = $pages[$current+1];

if (!empty($nextID)) { ?>
<p id="last"></p>
<div id="slidebox"><a class="btn-slide-in" href="<?php echo get_permalink($nextID); ?>"><?php echo get_the_title($nextID); ?></a></div>
<?php } else { ?>
<p id="last"></p>
<div id="slidebox"><a class="btn-slide-in" href="<?php echo get_permalink($pages[0]); ?>"><?php echo get_the_title($pages[0]); ?></a></div>
<?php
}
?> 
<?php endif; ?>
<?php if(get_field('slide_cta') ): ?>
	<p id="last"></p>
	<div id="slidebox"><a class="close">&nbsp;</a>
		<?php the_field('slide_cta'); ?>
	</div>
<?php elseif(get_field('global_slide_cta','option') ): ?>
	<p id="last"></p>
	<div id="slidebox"><a class="close">&nbsp;</a>
		<?php the_field('global_slide_cta','option'); ?>
	</div>
<?php endif; ?>