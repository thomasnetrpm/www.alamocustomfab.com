<section class="highlight-section">
  <div class="inner-wrap">
    <?php if( have_rows('project_highlight_slides') ): ?>
      <div class="flexslider">
        <ul class="slides">
          <?php while( have_rows('project_highlight_slides') ): the_row(); ?>
            <li>
              <h2 class="hls-title"><?php the_sub_field('hls_title'); ?></h2>

              <figure class="hls-img-block">
                <img class="slide" src="<?php the_sub_field('hls_image'); ?>" alt="" />
              </figure>

              <div class="hls-content-block">
                <h3 class="hls-subtitle"><?php the_sub_field('hls_subtitle'); ?></h3>
                <p class="hls-desc"><?php the_sub_field('hls_description'); ?></p>
                <a href="<?php the_sub_field('hls_learn_more_url'); ?>" class="btn">Learn More</a>
              </div>
            </li>
          <?php endwhile; ?>
          <!-- <li>
            <h2 class="hls-title">Project Highlights</h2>

            <figure class="hls-img-block">
              <img class="slide" src="<?php bloginfo('template_url') ?>/img/portfolio-tank.png" />
            </figure>

            <div class="hls-content-block">
              <h3 class="hls-subtitle">Son of a Gun</h3>
              <p class="hls-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe tempore, solut possimus nihil. Ratione ad quia doloribus autem culpa provident.</p>
              <a href="#" class="btn">Learn More</a>
            </div>

          </li>
          <li>
            <h2 class="hls-title">Project Highlights</h2>

            <figure class="hls-img-block">
              <img class="slide" src="<?php bloginfo('template_url') ?>/img/portfolio-tank.png" />
            </figure>

            <div class="hls-content-block">
              <h3 class="hls-subtitle">Son of a Gun</h3>
              <p class="hls-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae facere vitae  consequuntur sapiente cupiditate modi neque repudiandae! Sunt, accusantium, vel!</p>
              <a href="#" class="btn">Learn More</a>
            </div>

          </li>
          <li>
            <h2 class="hls-title">Project Highlights</h2>

            <figure class="hls-img-block">
              <img class="slide" src="<?php bloginfo('template_url') ?>/img/portfolio-tank.png" />
            </figure>

            <div class="hls-content-block">
              <h3 class="hls-subtitle">Son of a Gun</h3>
              <p class="hls-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus aut nemo, as voluptate rerum, consectetur cumque laudantium cum sint. Amet, laborum.</p>
              <a href="#" class="btn">Learn More</a>
            </div>

          </li> -->
        </ul>
      </div>
    <?php endif; ?>
  </div>
</section>