//PNG Fallback
if (!Modernizr.svg) {
  var images = $('img[data-png-fallback]');
  images.each(function(i) {
    $(this).attr('src', $(this).data('png-fallback'));
  });
}

//Toggle Boxes
$(document).ready(function() {
  $('body').addClass('js');
  var $activatelink = $('.activate-link');

  $activatelink.click(function() {
    var $this = $(this);
    $this.toggleClass('active').next('div').toggleClass('active');
    return false;
  });

});

//Responsive Navigation
$(document).ready(function() {
  $('body').addClass('js');
  var $menu = $('.site-nav-container'),
    $menulink = $('.menu-link'),
    $menuTrigger = $('.menu-item-has-children > a'),
    $searchLink = $('.search-link'),
    $siteSearch = $('.search-module'),
    $siteWrap = $('.site-wrap');

  $searchLink.click(function(e) {
    e.preventDefault();
    $searchLink.toggleClass('active');
    $siteSearch.toggleClass('active');
  });

  $menulink.click(function(e) {
    e.preventDefault();
    $menulink.toggleClass('active');
    $menu.toggleClass('active');
    $siteWrap.toggleClass('nav-active');
  });

  $menuTrigger.click(function(e) {
    //e.preventDefault();
    var $this = $(this);
    $this.toggleClass('active').next('ul').toggleClass('active');
  });

});

//Lightbox
$(document).ready(function() {
  $('.lightbox').magnificPopup({
    type: 'image'
  });
});
$(document).ready(function() {
  $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
    disableOn: 700,
    type: 'iframe',
    mainClass: 'mfp-fade',
    removalDelay: 160,
    preloader: false,

    fixedContentPos: false
  });
});



//Show More
$(document).ready(function() {
  $(".showmore").after("<p><a href='#' class='show-more-link'>More</a></p>");
  var $showmorelink = $('.showmore-link');
  $showmorelink.click(function() {
    var $this = $(this);
    var $showmorecontent = $('.showmore');
    $this.toggleClass('active');
    $showmorecontent.toggleClass('active');
    return false;
  });
});

//Show More
$(document).ready(function() {
  var $expandlink = $('.headexpand');
  $expandlink.click(function() {
    var $this = $(this);
    var $showmorecontent = $('.showmore');


    $this.toggleClass('active').next().toggleClass('active');
    $showmorecontent.toggleClass('active');
    return false;
  });
});


//Flexslider    
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: false,
    controlNav: false
  });
});


//Sticky Nav
$(function() {
 
    // grab the initial top offset of the navigation 
    var sticky_navigation_offset_top = $('.site-header').offset().top;

    // our function that decides weather the navigation bar should have "fixed" css position or not.
    var sticky_navigation = function(){
        var scroll_top = $(window).scrollTop(); // our current vertical position from the top
        // if we've scrolled more than the navigation, change its position to fixed to stick to top,
        // otherwise change it back to relative
        if (scroll_top > sticky_navigation_offset_top) { 
            $('.site-header').addClass('nav-sticky');
            $('.search-module').addClass('nav-sticky');
        } else {
            $('.site-header').removeClass('nav-sticky'); 
            $('.search-module').removeClass('nav-sticky');
        }   
    };
     
    // run our function on load
    sticky_navigation();
     
    // and run it again every time you scroll
    $(window).scroll(function() {
         sticky_navigation();
    });
 
});



//Slide in CTA
$(function() {
  var slidebox = $('#slidebox');
  if (slidebox) {
    $(window).scroll(function() {
      var distanceTop = $('#last').offset().top - $(window).height();
      if ($(window).scrollTop() > distanceTop)
        slidebox.animate({
          'right': '0px'
        }, 300);
      else
        slidebox.stop(true).animate({
          'right': '-930px'
        }, 100);
    });
    $('#slidebox .close').on('click', function() {
      $(this).parent().remove();
    });
  }
});
