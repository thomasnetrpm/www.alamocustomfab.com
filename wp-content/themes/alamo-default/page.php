<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
 
    
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
       
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<!--Site Content-->
	<section class="site-content" role="main">
			<div class="page-intro"></div>
	    <div class="inner-wrap">

				<h1>
					<?php if(get_field('alternative_h1')){
              echo get_field('alternative_h1');
          }
          else {
              the_title();
          }
          ?>
				</h1>
	      <?php the_content(); ?> 

	      <?php if (is_page( '8' )) : ?>
							<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/capabilities-section' ) ); ?>
						<?php endif; ?>     	
						
						<?php if (is_page( '309' )) : ?>
							<!--Sitemap Page-->
						   <ul>
						    <?php
						    // Add pages you'd like to exclude in the exclude here
						    wp_list_pages(
						    array(
						    'exclude' => '',
						    'title_li' => '',
						    )
						    );
						    ?>
						   </ul>
						<?php endif; ?>                    

		</div>
		<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/flexible-content' ) ); ?>
	</section>

<?php endwhile; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/slidebox' ) ); ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>